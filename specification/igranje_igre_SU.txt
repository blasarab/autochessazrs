﻿- Kratak opis: Igrač kupuje, prodaje i postavlja unite na board za vreme Shop faze. Kada Timer istekne, počinje Combat faza, gde se uniti koji su na boardu
	       bore sve dok vreme ne istekne ili dok jedna strana ne ostane bez unita. Na kraju Combata se igraču daje malo Golda i povećava broj unita koje 
               može da postavi na board. Igrač mora da pobedi 5 rundi protiv neprijateljskog boarda koji je generisan iz skupa već napravljenih boardova.
- Akteri: Igrač - igra dok ne pobedi, izgubi ili napusti igru
	  Bot - automatski generisan, igra dok igrač ne pobedi, izgubi ili napusti igru
- Preduslovi: Aplikacija je pokrenuta i pritisnuto je Start game u Main meniju
- Postuslovi: Informacije o igračevim unitima i statusu pobede su sačuvane
- Osnovni tok:
	1. Igrač započinje igru posle odabira podešavanja modova
	2. Aplikacija uzima informacije o podešavanjima igre
	3. Aplikacija kreira Shop
	4. Aplikacija krera Damage chart
	5. Aplikacija kreira Tajmer
	6. Aplikacija kreira Board
	7. Aplikacija kreira Bench
	8. Timer se resetuje i "Shop phase" traje sve dok ne istekne vreme
	  8.1. Aplikacija daje igraču 10 Golda i 2 mesta za unite na Boardu
	  8.2. Bot postavlja neprijateljski board
      	  8.3. Zavisno od moda u kome je igra pokrenuta, uniti se prikazuju ili bivaju skriveni
	  8.4. Igrač je u mogućnosti da ide podtokovima: "Kupi", "Prodaj", "Osveži", "Postavi unita na Board", "Pomeri unita na Bench"
	  8.5. Po isteku vremena aplikacija pamti imena i početne pozicije unita
	  8.6. Prelazi na korak 9
	9. Timer se resetuje i "Combat phase" traje sve dok ne istekne vreme ili dok jedan od timova ne izgubi
	  9.1. Aplikacija pokreće Combat fazu
	  9.2. Ako je isteklo vreme, pobeđuje ona strana sa vecom sumom levela heroja
		9.2.1. Ako obe strane imaju isto Unita, računa se kao da su obe strane izgubile
	  9.3. Ako tim ostane bez unita pre nego istekne vreme, taj tim je izgubio
	10. Aplikacija vraća Unite na pozicije pre bitke	
	11. Aplikacija pamti ishod bitke	  
	12. Ako je Igrač odigrao manje od 5 rundi, vraća se na korak 8
	13. Ako je Igrač odigrao 5 rundi, pokrećemo slučaj "Čuvanje istorije igara"
	14. Uništi Tajmer
	15. Uništi Board
	16. Uništi Bench
	17. Uništi Shop
	18. Završava se ovaj slučaj upotrebe i aplikacija se gasi
	
- Alternativni tokovi:
	A1: Neočekivani izlaz iz aplikacije. Ukoliko u bilo kom koraku korisnik isključi aplikaciju, istorija igre neće biti sačuvana.
- Podtokovi: 
	"Kupi":
	1. Igrač klikom na unita iz Shopa započinje podtok
	2. Ako Igrač ima para i ako postoji prazno mesto na Benchu:
	  2.1. Kliknuti Unit se briše iz Shopa
	  2.2. Kliknuti Unit se dodaje na prvo prazno mesto na Benchu
	  2.3. Aplikacija oduzima vrednost kupljenog Unita od Igračevih Golda

	"Prodaj":
	1. Igrač pritiskom tastera "S" dok je heroj selektovan započinje podtok
	2. Aplikacija briše Unita sa Bencha
	3. Aplikacija vraća Igraču 2/3 vrednosti prodanog Unita
	
	"Osvezi":
	1. Igrač pritiskom na dugme "Roll" u Shopu započinje podtok
	2. Ako Igrač ima 2 Golda:
	  2.1. Aplikacija postavlja nasumicnog unita na sva mesta u Shopu
	  2.2. Aplikacija oduzima 2 Golda od Igrača
	
	"Postavi unita na Board":
	1. Igrač prevlačenjem unita na polje Boarda zapocinje podtok
	2. Ako je polje na koje Igrač hoce da stavi Unita slobodno i ako ima mesta na Boardu:
	  2.1. Postavlja Unita na to polje
	
	"Pomeri unita na Bench":
	1. Igrač pritiskom na taster "W" dok je heroj selektovan započinje podtok
	2. Ako ima slobodnog mesta na Benchu, postavlja ga tamo
	
- Specijalni zahtevi: Nema specijalnih zahteva
- Dodatne informacije: Nema dodatnih informacija
