QT       += core gui \
            multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 \
          resources_big

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    code/bench.cpp \
    code/board.cpp \
    code/button.cpp \
    code/cell.cpp \
    code/controls.cpp \
    code/damagebar.cpp \
    code/damagechart.cpp \
    code/dreadherald.cpp \
    code/economy.cpp \
    code/elvenranger.cpp \
    code/game.cpp \
    code/graph.cpp \
    code/horseman.cpp \
    code/hpbar.cpp \
    code/knight.cpp \
    code/levelindicator.cpp \
    code/main.cpp \
    code/manabar.cpp \
    code/necromage.cpp \
    code/particle.cpp \
    code/particlemanager.cpp \
    code/philosopher.cpp \
    code/reaper.cpp \
    code/shop.cpp \
    code/stats.cpp \
    code/timer.cpp \
    code/uicell.cpp \
    code/unit.cpp \
    code/unitmanager.cpp \
    code/unitpool.cpp

HEADERS += \
    code/bench.h \
    code/board.h \
    code/button.h \
    code/cell.h \
    code/controls.h \
    code/damagebar.h \
    code/damagechart.h \
    code/dreadherald.h \
    code/economy.h \
    code/elvenranger.h \
    code/game.h \
    code/graph.h \
    code/horseman.h \
    code/hpbar.h \
    code/knight.h \
    code/levelindicator.h \
    code/manabar.h \
    code/necromage.h \
    code/particle.h \
    code/particlemanager.h \
    code/philosopher.h \
    code/reaper.h \
    code/shop.h \
    code/stats.h \
    code/timer.h \
    code/uicell.h \
    code/unit.h \
    code/unitName.h \
    code/unitVisitor.h \
    code/unitmanager.h \
    code/unitpool.h \
    code/utils.h

FORMS += \
    code/controls.ui \
    code/damagechart.ui \
    code/economy.ui \
    code/shop.ui \
    code/stats.ui \
    code/timer.ui



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES += \
    config/rounds.json \
    config/unitstats.json

