#include "uicell.h"
#include "cell.h"
#include "game.h"
#include <QGraphicsScene>
#include <QPainter>
#include <QString>
#include <QStyleOption>

UICell::UICell(QGraphicsObject *parent) : Cell::Cell(parent) {}
void UICell::mousePressEvent(QGraphicsSceneMouseEvent *event) {
	    Q_UNUSED(event)
}
void UICell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
		   QWidget *) {
	    Q_UNUSED(option)
	    painter->setBrush(_color);
	    painter->drawRect(0, 0, 96, 96);

	    if ( !_originalPixmap.isNull() ) {
			painter->drawPixmap(3, 3, 90, 90, _originalPixmap);
	    }
}
