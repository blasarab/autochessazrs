#ifndef UNITPOOL_H
#define UNITPOOL_H

#include "utils.h"
#include <QVector>

class UnitPool {
	    public:
	    UnitPool();

	    QVector<UnitType> pool;

	    void removeFromPool(qint32 i);
	    void addToPool(UnitType type);

	    private:
	    qint32 nrOfEachUnitInPool;
	    qint32 nrOfTypes;
};

#endif // UNITPOOL_H
