#include "dreadherald.h"
#include "game.h"

DreadHerald::DreadHerald(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::dreadherald, parent), _highlight(false) {
	    _originalPixmap = QPixmap(":/resources/images/dreadHerald.png");
	    _hlPixmap = QPixmap(":/resources/images/dreadHeraldHL.png");
	    setImage();
	    loadStats("DreadHerald");
	    hpBar = new HpBar(this, _team, _maxHp, _hp);
	    manaBar = new ManaBar(this, _team, _maxMana, _mana);
	    giveStatsForLevel();
	    Game::game()->addToScene(hpBar);
	    Game::game()->addToScene(manaBar);
	    hpBar->setVisible(false);
	    manaBar->setVisible(false);
}

void DreadHerald::drawUnit() {
	    setZValue(ZValues::zUnit);
	    Game::game()->addToScene(this);
	    if ( _team == Team::top &&
		 Game::game()->getGameMode() == HardMode::ON )
			hideUnit();
}

void DreadHerald::castSpell() {
	    if ( _silenced )
			return;
	    _mana = 0;
	    manaBar->setMana(0);
	    auto units = _team == Team::bottom
			   ? Game::game()->board->topUnits
			   : Game::game()->board->bottomUnits;

	    for ( auto unit : units ) {
			if ( !unit->_dead ) {
				    unit->_silenced = true;
				    unit->attackCooldown += 400;
				    unit->particleManager->receiveParticle(
					ParticleType::disable, _type,
					unit->pos());
			}
	    }

	    duration = new QTimer(this);
	    duration->setInterval(1000 * _spellDuration);

	    duration->start();
	    connect(duration, &QTimer::timeout, this, [&] {
			auto units = _team == Team::bottom
				       ? Game::game()->board->topUnits
				       : Game::game()->board->bottomUnits;
			for ( auto unit : units ) {
				    if ( !unit->_dead ) {
						unit->_silenced = false;
						unit->attackCooldown =
						    unit->originalAttackCooldown;
						unit->particleManager
						    ->clearDisableParticles();
				    }
			}
			duration->stop();
	    });
}

void DreadHerald::giveStatsForLevel() {
	    _maxHp = _hpPerLevel * (_level + 1);
	    _hp = _maxHp;
	    hpBar->setMaxHp(_maxHp);

	    _spellDuration = 0.5 * (_level + 6.0);
	    _spellText = "Dread Herald silences(unable to cast spells) and "
			 "slows enemy attack speed for 0.4 attacks/sec for " +
			 QString::number(_spellDuration, 'f', 1) + " seconds";
	    _damage = _dmgPerLevel * (_level + 1);
}
