#include "graph.h"
#include <QMap>
#include <QStack>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <set>

#include <chrono>
#include <iostream>

//#define ASTAR_TIMETEST

Graph::Graph() {
	    // TODO IZDODAVATI & SVUDA U ARG FJ
	    matrix =
		std::vector<std::vector<bool>>(8, std::vector<bool>(8, false));
}

qint32 distance(Node from, Node to) {
	    return std::max(abs(from.first - to.first),
			    abs(from.second - to.second));
}

// currentI < 0 || currentI > 7 || currentJ < 0 || currentJ > 7
// isvalid
// is taken
// getclosestenemy
qint32 Graph::h(Node from, Node to) { return distance(from, to); }
std::vector<Node> Graph::getNeighbors(Node node) {
	    std::vector<Node> neighbors;

	    auto row = node.first;
	    auto col = node.second;

	    // znam da je c-oliko ali Too bad!
	    for ( auto i = -1; i <= 1; i++ ) {
			for ( auto j = -1; j <= 1; j++ ) {
				    if ( i == 0 && j == 0 )
						continue;
				    auto currentI = row + i;
				    auto currentJ = col + j;

				    // if out of bounds
				    if ( currentI < 0 || currentI > 7 ||
					 currentJ < 0 || currentJ > 7 )
						continue;
				    // if taken
				    if ( matrix[currentI][currentJ] )
						continue;

				    neighbors.push_back(
					Node(currentI, currentJ));
			}
	    }

	    return neighbors;
}
Node Graph::astar(Node start, Node stop) {

#ifdef ASTAR_TIMETEST
	    auto startTime = std::chrono::high_resolution_clock::now();
#endif

	    std::set<Node> openList({start});
	    std::set<Node> closedList;

	    std::map<Node, qint32> g;
	    std::map<Node, Node> parents;

	    // if zauzet kraj
	    //    if (matrix[stop.first][stop.second] == true)
	    //        return NotFound;

	    // MOZDA OPASNO::: SETOVANJE MATRICE NA NEOBELEZENO
	    // PA KASNIJE RESETOVANJE
	    // ********************************************

	    bool stopTaken = matrix[stop.first][stop.second];
	    matrix[stop.first][stop.second] = false;

	    // *****************************

	    g[start] = 0;
	    parents[start] = start;
	    Node startChild = NotFound;
	    Node n;

	    while ( !openList.empty() ) {
			n = NotFound;

			for ( auto &v : openList ) {
				    if ( n == NotFound ||
					 g[v] + h(v, stop) <
					     g[n] + h(n, stop) ) {
						n = v;
				    }
			}
			if ( n == NotFound ) {
				    qDebug("PATH DOES NOT EXIST");
				    matrix[stop.first][stop.second] = stopTaken;
				    return NotFound;
			}
			if ( n == stop ) {

				    while ( parents[n] != n ) {
						if ( parents[n] == start &&
						     (startChild == NotFound ||
						      g[n] < g[startChild]) )
							    startChild = n;
						n = parents[n];
				    }

#ifdef ASTAR_TIMETEST
				    // CHRONO
				    auto stopTime = std::chrono::
					high_resolution_clock::now();
				    auto duration = std::chrono::duration_cast<
					std::chrono::microseconds>(stopTime -
								   startTime);

				    std::cout << "Time taken by function: "
					      << duration.count()
					      << " microseconds" << std::endl;
#endif
				    // MOZDA OPASNO::::::
				    matrix[stop.first][stop.second] = stopTaken;
				    // *****************

				    return startChild;
			}

			for ( auto &m : getNeighbors(n) ) {
				    if ( openList.find(m) == openList.end() &&
					 closedList.find(m) ==
					     closedList.end() ) {
						openList.insert(m);
						parents[m] = n;
						g[m] = g[n] + 1;
				    } else {
						if ( g[m] > g[n] + 1 ) {
							    g[m] = g[n] + 1;
							    parents[m] = n;

							    if ( closedList
								     .find(m) !=
								 closedList
								     .end() ) {
									closedList
									    .erase(
										m);
									openList
									    .insert(
										m);
							    }
						}
				    }
			}

			openList.erase(n);
			closedList.insert(n);
	    }

	    return NotFound;
}
