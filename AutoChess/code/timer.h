#ifndef TIMER_H
#define TIMER_H
#include "utils.h"
#include <QGraphicsProxyWidget>
#include <QString>
#include <QTimer>
#include <QWidget>

namespace Ui {
class Timer;
}

class Timer : public QWidget {
	    Q_OBJECT

	    public:
	    explicit Timer(QWidget *parent = nullptr, quint32 shopDuration = 0,
			   quint32 combatDuration = 0);
	    ~Timer();
	    QGraphicsProxyWidget *proxyParent;
	    void drawTimer();

	    QTimer *shopGuiTimer;
	    QTimer *combatTick;
	    QTimer *combatGuiTimer;

	    void updateTimerColor(Phase phase);
	    void resetTime(Phase phase);
	    quint32 getTime();

	    private:
	    quint32 _shopPhaseDuration;
	    quint32 _combatPhaseDuration;
	    quint32 _time;
	    Ui::Timer *ui;
};

#endif // TIMER_H
