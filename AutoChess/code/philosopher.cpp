#include "philosopher.h"
#include "game.h"
#include "utils.h"
#include <iostream>

Philosopher::Philosopher(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::philosopher, parent), _highlight(false) {
	    _hlPixmap = QPixmap(":/resources/images/thePhilosopherHL.png");
	    _originalPixmap = QPixmap(":/resources/images/thePhilosopher.png");
	    setImage();
	    loadStats("Philosopher");
	    hpBar = new HpBar(this, _team, _maxHp, _hp);
	    manaBar = new ManaBar(this, _team, _maxMana, _mana);
	    giveStatsForLevel();
	    Game::game()->addToScene(hpBar);
	    Game::game()->addToScene(manaBar);
	    hpBar->setVisible(false);
	    manaBar->setVisible(false);
}

void Philosopher::drawUnit() {
	    setZValue(ZValues::zUnit);
	    Game::game()->addToScene(this);
	    if ( _team == Team::top &&
		 Game::game()->getGameMode() == HardMode::ON )
			hideUnit();
}

void Philosopher::castSpell() {
	    if ( _casted == true )
			return;

	    if ( _team == Team::bottom ) {
			for ( auto unit : Game::game()->board->bottomUnits ) {
				    unit->_spellDamage += _spellBoost;
				    if ( unit->_manaPerHit != 0 )
						unit->_manaPerHit += 5;
			}
	    } else if ( _team == Team::top ) {
			for ( auto unit : Game::game()->board->topUnits ) {
				    unit->_spellDamage += _spellBoost;
				    if ( unit->_manaPerHit != 0 )
						unit->_manaPerHit += 5;
			}
	    }

	    _casted = true;
}

void Philosopher::giveStatsForLevel() {
	    _casted = false;
	    _maxHp = _hpPerLevel * (_level + 1);
	    _hp = _maxHp;
	    hpBar->setMaxHp(_maxHp);

	    _spellBoost = 20 * (_level + 1);
	    _spellText = "Philosopher increases mana gain of all teammates and "
			 "empowers teammates spell damage by " +
			 QString::number(_spellBoost);
	    _damage = _dmgPerLevel * (_level + 1);
}
