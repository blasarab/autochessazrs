#ifndef SHOP_H
#define SHOP_H

#include "cell.h"
#include "uicell.h"
#include "unitName.h"
#include "unitpool.h"
#include "utils.h"
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QLabel>
#include <QWidget>
#include <vector>

namespace Ui {
class Shop;
}

class Shop : public QWidget {
	    Q_OBJECT

	    public:
	    explicit Shop(QWidget *parent = nullptr);
	    ~Shop();

	    QVector<Unit *> shopUnits;
	    UICell *shopCells[6];
	    QLabel *nameShopHero[6];
	    QLabel *priceShopHero[6];

	    UnitPool *unitPool;
	    QGraphicsProxyWidget *proxyParent;

	    void drawShop();
	    void clearUnits();
	    void labelRefresh();

	    void rollShopUnits();

	    void removeFromShopUnits(Unit *unit);

	    void noMoneyForUnitMsg();
	    private slots:
	    void on_btnRoll_clicked();

	    private:
	    qint32 rollprice_ = 2;
	    Ui::Shop *ui;
	    quint32 _unitsBought;
};

#endif // SHOP_H
