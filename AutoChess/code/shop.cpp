#include "shop.h"
#include "game.h"
#include "ui_shop.h"
#include <QMessageBox>
#include <iostream>

#include <QPainter>

Shop::Shop(QWidget *parent) : QWidget(parent), ui(new Ui::Shop) {
	    ui->setupUi(this);
	    move(-50, -85);

	    unitPool = new UnitPool();
	    _unitsBought = 0;
}

void Shop::drawShop() {
	    ui->lRollPrice->setText(QString::number(rollprice_) + "G: ");

	    for ( auto i = 0; i < 6; ++i ) {
			shopCells[i] = new UICell(nullptr);
			UICell *cell = shopCells[i];
			cell->moveBy(-21 + (i % 3) * 123, 30);
			if ( i > 2 )
				    cell->moveBy(0, 192);
			cell->setTaken(false);
			cell->setColor(Qt::lightGray);
			cell->setOriginalColor(Qt::lightGray);
	    }

	    proxyParent = Game::game()->addWidget(this);
	    for ( unsigned i = 0; i < 6; ++i ) {
			Game::game()->addToScene(shopCells[i]);
	    }
	    labelRefresh();
}

void Shop::labelRefresh() {
	    if ( shopCells[0]->isTaken() ) {
			ui->lNameShopHero_1->setText(
			    UnitName(*(shopUnits[0])).name_);
			ui->lPriceShopHero_1->setText("1G");
	    } else {
			ui->lNameShopHero_1->setText("");
			ui->lPriceShopHero_1->setText("");
	    }
	    //---------------------------------------------------------------
	    if ( shopCells[1]->isTaken() ) {
			ui->lNameShopHero_2->setText(
			    UnitName(*(shopUnits[1])).name_);
			ui->lPriceShopHero_2->setText("1G");
	    } else {
			ui->lNameShopHero_2->setText("");
			ui->lPriceShopHero_2->setText("");
	    }
	    //---------------------------------------------------------------
	    if ( shopCells[2]->isTaken() ) {
			ui->lNameShopHero_3->setText(
			    UnitName(*(shopUnits[2])).name_);
			ui->lPriceShopHero_3->setText("1G");
	    } else {
			ui->lNameShopHero_3->setText("");
			ui->lPriceShopHero_3->setText("");
	    }
	    //---------------------------------------------------------------
	    if ( shopCells[3]->isTaken() ) {
			ui->lNameShopHero_4->setText(
			    UnitName(*(shopUnits[3])).name_);
			ui->lPriceShopHero_4->setText("1G");
	    } else {
			ui->lNameShopHero_4->setText("");
			ui->lPriceShopHero_4->setText("");
	    }
	    //---------------------------------------------------------------
	    if ( shopCells[4]->isTaken() ) {
			ui->lNameShopHero_5->setText(
			    UnitName(*(shopUnits[4])).name_);
			ui->lPriceShopHero_5->setText("1G");
	    } else {
			ui->lNameShopHero_5->setText("");
			ui->lPriceShopHero_5->setText("");
	    }
	    //---------------------------------------------------------------
	    if ( shopCells[5]->isTaken() ) {
			ui->lNameShopHero_6->setText(
			    UnitName(*(shopUnits[5])).name_);
			ui->lPriceShopHero_6->setText("1G");
	    } else {
			ui->lNameShopHero_6->setText("");
			ui->lPriceShopHero_6->setText("");
	    }
}

Shop::~Shop() { delete ui; }

void Shop::clearUnits() {
	    for ( auto unit : shopUnits ) {

			if ( unit->getParentCell()->getLocation() ==
			     Location::shop ) {
				    unitPool->addToPool(unit->getType());
				    delete unit;
			}
	    }

	    shopUnits.clear();
}

void Shop::on_btnRoll_clicked() {
	    if ( Game::game()->economy->gold >= rollprice_ ) {
			Game::game()->economy->gold -= rollprice_;
			Game::game()->economy->updateGold();
	    }

	    else {
			QMessageBox msgBox;
			msgBox.setText("Not enough gold for ROLL!");
			msgBox.exec();
			qDebug("NOT ENOUGH GOLD FOR ROLL");
			return;
	    }

	    rollShopUnits();
}

void Shop::rollShopUnits() {
	    clearUnits();
	    srand(time(nullptr));
	    for ( Cell *cell : shopCells ) {
			unsigned random = rand() % unitPool->pool.length();
			cell->setTaken(true);
			cell->setLocation(Location::shop);
			cell->update();

			Team team = Team::bottom;
			UnitType type = unitPool->pool[random];
			unitPool->removeFromPool(random);
			Unit *newUnit = Unit::create(type, team, 1);

			newUnit->setParentCell(cell);
			newUnit->drawUnit();
			shopUnits.append(newUnit);
	    }
	    _unitsBought = 0;
	    labelRefresh();
}

void Shop::removeFromShopUnits(Unit *unit) {
	    auto unitIt = std::find(shopUnits.begin(), shopUnits.end(), unit);
	    if ( unitIt == shopUnits.end() )
			return;

	    _unitsBought++;
	    quint32 d = std::distance(shopUnits.begin(), unitIt);

	    shopUnits.remove(d);
	    shopCells[6 - _unitsBought]->setTaken(false);

	    for ( auto i = d; i < 6 - _unitsBought; i++ ) {
			Cell *cell = shopCells[i];
			Unit *u = shopUnits[i];
			u->setParentCell(cell);
			u->getParentCell()->setTaken(true);
			u->update();
	    }
}

void Shop::noMoneyForUnitMsg() {
	    QMessageBox msgBox;
	    msgBox.setText("Not enough gold to buy unit!");
	    msgBox.exec();
}
