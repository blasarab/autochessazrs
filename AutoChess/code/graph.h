#ifndef GRAPH_H
#define GRAPH_H

#include <QList>
#include <QVector3D>
#include <vector>

using Node = QPair<qint32, qint32>;
const Node NotFound = Node(-1, -1);

class Graph {
	    public:
	    Graph();
	    ~Graph();

	    std::vector<std::vector<bool>> matrix;

	    std::vector<Node> getNeighbors(Node node);
	    // qint32 distance(Node from, Node to);
	    Node astar(Node from, Node to);

	    qint32 h(Node from, Node to);

	    // SET TAKEN OBUHVACEN U PLACEUNIT
};

qint32 distance(Node from, Node to);
#endif // GRAPH_H
