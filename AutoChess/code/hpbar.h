#ifndef HPBAR_H
#define HPBAR_H

#include "utils.h"
#include <QGraphicsObject>

class HpBar : public QGraphicsObject {
	    public:
	    HpBar(QGraphicsObject *parent, Team team, qint32 maxHp, qint32 hp);
	    QRectF boundingRect() const override;
	    void paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *option,
		       QWidget *) override;

	    void setMaxHp(qint32 newMaxHp);

	    qint32 hp() const;
	    void setHp(qint32 newHp);

	    private:
	    Team _team;
	    qint32 _maxHp;
	    qint32 _hp;
	    QColor _color;
};

#endif // HPBAR_H
