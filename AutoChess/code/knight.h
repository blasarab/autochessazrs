#ifndef KNIGHT_H
#define KNIGHT_H

#include "unit.h"
#include "utils.h"

class Knight : public Unit {
	    public:
	    Knight(Team team, quint32 level, QGraphicsItem *parent = 0);

	    void drawUnit() override;
	    void setHighlight(bool hl);
	    bool getHighlight();
	    void acceptVisit(UnitVisitor &visitor) override {
			visitor.visitKnight(*this);
	    };

	    void castSpell() override;

	    void giveStatsForLevel() override;

	    void updateSpellText() override;

	    private:
	    bool _highlight;
};

#endif // KNIGHT_H
