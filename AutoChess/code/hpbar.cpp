#include "hpbar.h"
#include "game.h"
#include <QPainter>

HpBar::HpBar(QGraphicsObject *parent, Team team, qint32 maxHp, qint32 hp) {
	    if ( team == Team::bottom ) {
			_color = Qt::darkGreen;
	    } else {
			_color = Qt::darkRed;
	    }
	    setPos(parent->pos());
	    setZValue(ZValues::zHpBar);
	    _maxHp = maxHp;
	    _hp = hp;
}

QRectF HpBar::boundingRect() const { return QRectF(0, 0, 90, 6); }

void HpBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
		  QWidget *) {
	    Q_UNUSED(option)

	    // TODO BOJE ZA TIMOVE
	    painter->setBrush(Qt::black);
	    painter->drawRect(0, 0, 90, 6);
	    painter->setBrush(_color);
	    painter->drawRect(0, 0, _hp * 90 / _maxHp, 6);
}

void HpBar::setMaxHp(qint32 newMaxHp) {
	    _maxHp = newMaxHp;
	    _hp = _maxHp;
}

qint32 HpBar::hp() const { return _hp; }

void HpBar::setHp(qint32 newHp) {
	    _hp = newHp;
	    if ( _hp > _maxHp )
			_hp = _maxHp;
	    update();
}
