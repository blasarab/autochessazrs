#ifndef STATS_H
#define STATS_H

#include "uicell.h"
#include "unit.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QString>
#include <QWidget>

namespace Ui {
class Stats;
}

class Stats : public QWidget {
	    Q_OBJECT

	    public:
	    explicit Stats(QWidget *parent = nullptr);
	    ~Stats();
	    UICell *statsCell;
	    QGraphicsProxyWidget *proxyParent;
	    void updateStats(Unit *selectedUnits);
	    void drawStats();
	    public slots:
	    void fillStats(Unit *selectedUnit);
	    void clearStats();

	    private:
	    Ui::Stats *ui;
};

#endif // STATS_H
