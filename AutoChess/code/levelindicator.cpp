#include "levelindicator.h"
#include "unit.h"
#include <QPainter>

LevelIndicator::LevelIndicator(qint32 levelValue, QGraphicsObject *parent)
    : QGraphicsObject(parent) {
	    setParent(parent);

	    _levelValue = levelValue;

	    setZValue(ZValues::zLevelIndicator);
	    setVisible(false);

	    _sideLen = 24;
	    _offset = QPoint(90 - _sideLen, 2);

	    setPos(parent->pos() + _offset);
	    setImage();
}

void LevelIndicator::setImage() {
	    switch ( _levelValue ) {
			case 1:
				    _pixmap = QPixmap(
					":/resources/images/level1.png");
				    break;
			case 2:
				    _pixmap = QPixmap(
					":/resources/images/level2.png");
				    break;
			case 3:
				    _pixmap = QPixmap(
					":/resources/images/level3.png");
				    break;
			case 4:
				    _pixmap = QPixmap(
					":/resources/images/level4.png");
				    break;
			case 5:
				    _pixmap = QPixmap(
					":/resources/images/level5.png");
				    break;
			default:
				    _pixmap = QPixmap(
					":/resources/images/level1.png");
	    }
	    _scaledPixmap = QPixmap(
		_pixmap.scaled(_sideLen, _sideLen, Qt::KeepAspectRatio));
}

QRectF LevelIndicator::boundingRect() const {

	    return QRectF(0, 0, _sideLen, _sideLen);
}

void LevelIndicator::paint(QPainter *painter,
			   const QStyleOptionGraphicsItem *option, QWidget *) {

	    Q_UNUSED(option)
	    painter->drawPixmap(0, 0, _sideLen, _sideLen, _scaledPixmap);
}

qint32 LevelIndicator::levelValue() const { return _levelValue; }

void LevelIndicator::setLevel(qint32 newLevelValue) {
	    _levelValue = newLevelValue;
	    setImage();
	    update();
}

void LevelIndicator::refresh() {
	    Unit *u = dynamic_cast<Unit *>(parent());
	    QPointF pos = u->pos();
	    pos += _offset;
	    setPos(pos);
	    setLevel(u->_level);
	    setVisible(true);
	    update();
}
