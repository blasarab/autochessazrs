#ifndef UNITVISITOR_H
#define UNITVISITOR_H

class Knight;
class Reaper;
class Necromage;
class DreadHerald;
class Philosopher;
class Horseman;
class ElvenRanger;

class UnitVisitor {
	    public:
	    UnitVisitor() {}
	    ~UnitVisitor() {}
	    virtual void visitKnight(Knight &) = 0;
	    virtual void visitReaper(Reaper &) = 0;
	    virtual void visitNecromage(Necromage &) = 0;
	    virtual void visitDreadHerald(DreadHerald &) = 0;
	    virtual void visitPhilosopher(Philosopher &) = 0;
	    virtual void visitHorseman(Horseman &) = 0;
	    virtual void visitElvenRanger(ElvenRanger &) = 0;
};

#endif // UNITVISITOR_H
