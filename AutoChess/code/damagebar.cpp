#include "damagebar.h"
#include "game.h"
#include <QGraphicsTextItem>
#include <QPainter>

DamageBar::DamageBar(QGraphicsObject *parent, Team team, QString name,
		     QString level)
    : QGraphicsObject(parent), _team(team), _name(name), _lvl(level) {
	    if ( team == Team::bottom ) {
			_color = Qt::yellow;
	    } else {
			_color = Qt::cyan;
	    }
	    setPos(0, 0);
	    setZValue(ZValues::zDamageBar);
	    _maxDamageDealt = 0;
	    _damageDealt = 0;
	    _text = new QGraphicsTextItem(_name + " " + _lvl, this);
	    _text->setDefaultTextColor(Qt::yellow);
	    _text->setZValue(ZValues::zDamageText);
	    _damageText =
		new QGraphicsTextItem(QString::number(_damageDealt), this);
	    _damageText->setDefaultTextColor(Qt::black);
	    _damageText->setZValue(ZValues::zDamageText);
	    setVisible(false);
	    Game::game()->addToScene(this);
}

DamageBar::~DamageBar() { Game::game()->removeFromScene(this); }

QRectF DamageBar::boundingRect() const {
	    return QRectF(0, 0, DamageBarSize::width, DamageBarSize::height);
}

void DamageBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
		      QWidget *) {
	    Q_UNUSED(option)

	    painter->setBrush(_color);
	    if ( _maxDamageDealt != 0 )
			painter->drawRect(0, 0,
					  _damageDealt * DamageBarSize::width /
					      _maxDamageDealt,
					  DamageBarSize::height);
	    else
			painter->drawRect(0, 0, 1, DamageBarSize::height);
}

void DamageBar::addDamageDealt(qint32 damageDealt) {
	    _damageDealt += damageDealt;
	    if ( _damageDealt > _maxDamageDealt ) {
			Game::game()->damageChart->updateDamageChart(
			    _team, _damageDealt);
	    }
	    updateText();
}

qint32 DamageBar::damageDealt() const { return _damageDealt; }

void DamageBar::setMaxDamageDealt(qint32 newMaxDamageDealt) {
	    _maxDamageDealt = newMaxDamageDealt;
	    update();
}

void DamageBar::resetDamageDealt() {
	    _damageDealt = 0;
	    _maxDamageDealt = 0;
	    updateText();
}

void DamageBar::updateText() {
	    _text->setPlainText(_name + " " + _lvl);
	    _damageText->setPlainText(QString::number(_damageDealt));
	    update();
}

void DamageBar::setTextPos() {
	    _text->setPos(-120, -3);
	    _damageText->setPos(5, -3);
	    update();
}

void DamageBar::setLvl(QString level) {
	    _lvl = level;
	    updateText();
}
