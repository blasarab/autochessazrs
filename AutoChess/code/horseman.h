#ifndef HORSEMAN_H
#define HORSEMAN_H

#include "unit.h"
#include "utils.h"
#include <QTimer>

class Horseman : public Unit {
	    public:
	    Horseman(Team team, quint32 level, QGraphicsItem *parent = 0);

	    QTimer *stunDuration;
	    void drawUnit() override;
	    void setHighlight(bool hl);
	    bool getHighlight();
	    void acceptVisit(UnitVisitor &visitor) override {
			visitor.visitHorseman(*this);
	    };

	    bool takeDamage(qint32 dmg) override;
	    void attack() override;

	    void castSpell() override;

	    void giveStatsForLevel() override;

	    void updateSpellText() override;

	    void stunTarget();

	    private:
	    bool _highlight;
	    float _dmgReduction;
	    quint32 _hitCount;
	    float _stunTime;
};

#endif // HORSEMAN_H
