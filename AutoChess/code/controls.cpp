#include "controls.h"
#include "game.h"
#include "ui_controls.h"

Controls::Controls(QWidget *parent) : QWidget(parent), ui(new Ui::Controls) {
	    ui->setupUi(this);
	    move(-50, -85 + 460 + 12 + 280 + 12);
}

void Controls::drawControls() { proxyParent = Game::game()->addWidget(this); }

Controls::~Controls() { delete ui; }
