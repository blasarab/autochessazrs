#include "game.h"

#include <QApplication>
#include <QColor>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QtGui>

Unit *selectedUnit = nullptr;
Cell *selectedCell = nullptr;

int main(int argc, char *argv[]) {
	    QApplication a(argc, argv);

	    Game *game = Game::game();
	    game->show();
	    game->mainMenu();
	    return a.exec();
}
