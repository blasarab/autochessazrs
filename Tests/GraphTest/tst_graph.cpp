#define CATCH_CONFIG_MAIN
#include "../../AutoChess/code/graph.h"
#include "../catch.hpp"
#include <iostream>

TEST_CASE("Graph matrix", "[init]") {

	    Graph *g = new Graph();

	    bool val = false;

	    for ( auto i = 0; i < 8; i++ )
			for ( auto j = 0; j < 8; j++ )
				    if ( g->matrix[i][j] )
						val = true;

	    REQUIRE(val == false);
}
TEST_CASE("getNeighbors") {
	    SECTION("All possible neighbors") {
			Graph *g = new Graph();

			for ( auto i1 = 0; i1 < 8; i1++ ) {
				    for ( auto j1 = 0; j1 < 8; j1++ ) {
						Node n(i1, j1);

						auto size =
						    g->getNeighbors(n).size();
						REQUIRE(size >= 3);
						REQUIRE(size <= 8);
				    }
			}
	    }
}
TEST_CASE("Astar", "[progress]") {
	    SECTION("All possible valid nodes") {
			Graph *g = new Graph();

			for ( auto i1 = 0; i1 < 8; i1++ ) {
				    for ( auto j1 = 0; j1 < 8; j1++ ) {
						for ( auto i2 = 0; i2 < 8;
						      i2++ ) {
							    for ( auto j2 = 0;
								  j2 < 8;
								  j2++ ) {

									Node start(
									    i1,
									    j1);
									Node finish(
									    i2,
									    j2);

									if (
									    start ==
									    finish )
										    continue;

									Node nextStep =
									    g->astar(
										start,
										finish);
									REQUIRE(
									    distance(
										start,
										nextStep) <=
									    1);
									REQUIRE(
									    distance(
										nextStep,
										finish) <=
									    distance(
										start,
										finish));
							    }
						}
				    }
			}
	    }
	    SECTION("Astar", "blocked path") {
			Graph *g = new Graph();

			Node start(0, 0);
			Node finish(7, 7);

			g->matrix[0][1] = g->matrix[1][0] = g->matrix[1][1] =
			    true;

			Node nextStep = g->astar(start, finish);

			REQUIRE(nextStep == NotFound);
	    }
}
