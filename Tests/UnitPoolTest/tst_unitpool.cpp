#define CATCH_CONFIG_MAIN
#include "../../AutoChess/code/unitpool.h"
#include "../catch.hpp"
#include <iostream>

TEST_CASE("Iniciranje unit poola", "[init]") {

	    SECTION("Construktor pravi unit pool duzine 140") {
			UnitPool *unitpool = new UnitPool();

			const qint32 duzinaPoola = 20 * 7;

			REQUIRE(duzinaPoola == unitpool->pool.length());
	    }

	    SECTION("Construktor pravi unit pool sa 20 od svakog tipa unita") {
			UnitPool *unitpool = new UnitPool();

			qint32 dvadesetOdSvakog = true;

			for ( qint32 j = 0; j < 7; j++ ) {
				    for ( qint32 i = 1; i < 20; i++ ) {
						if ( unitpool->pool[i] !=
						     unitpool->pool[i - 1] ) {
							    dvadesetOdSvakog =
								false;
							    break;
						}
				    }
			}
			REQUIRE(dvadesetOdSvakog == true);
	    }
}

TEST_CASE("Micanje unita iz poola", "[remove from pool]") {

	    SECTION("Uklanjanje 3 unita, smanjuje duzinu poola za 3") {
			UnitPool *unitpool = new UnitPool();

			const qint32 duzinaPoola = 20 * 7;
			unitpool->pool.remove(1);
			unitpool->pool.remove(1);
			unitpool->pool.remove(1);

			REQUIRE(duzinaPoola - 3 == unitpool->pool.length());
	    }

	    SECTION("Uklanjanje prvih 20 unita, skroz uklanja taj tip unita iz "
		    "poola") {
			UnitPool *unitpool = new UnitPool();

			const UnitType unittype = unitpool->pool[0];
			bool unitRemoved = true;

			for ( qint32 j = 0; j < 20; j++ ) {
				    unitpool->pool.remove(0);
			}

			for ( auto type : unitpool->pool ) {
				    if ( type == unittype )
						unitRemoved = false;
			}

			REQUIRE(unitRemoved == true);
	    }
}

TEST_CASE("Dodavanje unita u pool", "[add to pool]") {

	    SECTION("Dodavanjem 3 unita, povecava duzinu poola za 3") {
			UnitPool *unitpool = new UnitPool();

			const qint32 duzinaPoola = 20 * 7;
			unitpool->addToPool(UnitType::sunknight);
			unitpool->addToPool(UnitType::sunknight);
			unitpool->addToPool(UnitType::sunknight);

			REQUIRE(duzinaPoola + 3 == unitpool->pool.length());
	    }

	    SECTION("Dodavanjem jos 5 unita tipa \"sunknight\", bice ih 25 u "
		    "unit poolu") {
			UnitPool *unitpool = new UnitPool();

			const UnitType unittype = UnitType::sunknight;
			const qint32 brojSunknightova = 25;
			qint32 brojac = 0;

			for ( qint32 j = 0; j < 5; j++ ) {
				    unitpool->addToPool(unittype);
			}

			for ( auto type : unitpool->pool ) {
				    if ( type == unittype )
						brojac++;
			}

			REQUIRE(brojac == brojSunknightova);
	    }
}
